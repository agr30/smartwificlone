import esptool
import sys
import tempfile

flashCommand = []
mainBinaryLoc = str(sys.argv[2])
portBoard = sys.argv[1]
temp = tempfile.NamedTemporaryFile(mode='r+b')
macValue = ''

def buildFlashCommand(arg, mnDir):
    try:
        if arg != "":
            flashCommand = ['-p', str(arg), 'write-flash', '--flash-freq', '26m', '--flash_mode', 'dio', '--no-compress', '0x0', mnDir]
        else:
            raise Exception()
    except:
        print("You must enter the port of WeMos D1 mini")

def buildSpecificFlashCmd(arg, tmp):
    try:
        if arg != "":
            flashCommand = ['-p', str(arg), 'write-flash', '--flash-freq', '26m', '--flash_mode', 'dio', '--no-compress', '0x3fb000', str(tmp.name)]
        else:
            raise Exception()
    except:
        print("Can't find the temporary file for flashing specific firmware")

def eraseBoard(arg):
    try:
        esptool.main(['-p', str(arg), 'erase-flash' ])
    except:
        print("Board flash can't be erased")

def createSpecificFile(arg):
    try:
        with file('3fb000sector.bin', 'rb') as fh:
            patch = fh.read()
            temp.write(patch)
        with file(str(temp.name), 'r+b') as fh:
            fh.seek(0x0000000D)
            fh.write(arg)
    except:
        print("Can't Create Specific File for board")

def showMac(arg):
    try:
        esptool.main(['-p', str(arg), 'flash_id'])
    except:
        print("Can't connect to Board. Please check if it's good connected and port is correct")

def setMac():
    macValue = input("Please, read the 2 last digits of Address MAC above and input it with 0x followed by MAC address:")

print("Starting flash of: "+ portBoard)
print("Launching Erase flash:")
eraseBoard(portBoard)

print("Board flash has been erased correctly")
print("Reading flash ID of board:")
showMac(portBoard)
setMac()

print("Building temporary specific file for firmware")
createSpecificFile(macValue)

print("Flashing specific firmware for board:")
buildFlashCommand(portBoard, mainBinaryLoc)
esptool.main(flashCommand)
buildSpecificFlashCmd(portBoard, temp)
esptool.main(flashCommand)

print("Board has been flashed correctly! Enjoy your SmartShow lighting")
temp.close()
